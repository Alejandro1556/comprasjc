package com.example.comprasjc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.comprasjc.models.Product;

import java.util.UUID;

import io.realm.Realm;

public class EditProductActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etDescription;
    private EditText etQuantify;
    private EditText etValue;
    private Button btnSave;

    private Realm realm;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.editProductTitle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),ProductActivity.class));
                }
            });
        }
        realm = Realm.getDefaultInstance();
        loadViews();
        id = getIntent().getStringExtra("productId");
        loadProduct(id);
    }

    private void loadProduct(String id) {
        Product product = realm.where(Product.class).equalTo("id", id).findFirst();
        if (product != null) {
            etName.setText(product.getName());
            etDescription.setText(product.getDescription());
            etQuantify.setText(String.valueOf(product.getQuantify()));
            etValue.setText(String.valueOf(product.getValue()));
        }
    }

    private void loadViews() {
        etName = findViewById(R.id.et_name);
        etDescription = findViewById(R.id.et_description);
        etQuantify = findViewById(R.id.et_quantify);
        etValue = findViewById(R.id.et_value);
        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct();
            }
        });
    }

    private void updateProduct() {
        String name = etName.getText().toString();
        String description = etDescription.getText().toString();
        String strQuantity = etQuantify.getText().toString().trim();
        String strValue = etValue.getText().toString().trim();
        boolean isSuccess = true;

        if (name.isEmpty()) {
            String message = getString(R.string.required);
            etName.setError(message);
            isSuccess = false;
        }
        if (description.isEmpty()) {
            String message = getString(R.string.required);
            etDescription.setError(message);
            isSuccess = false;
        }
        if (strQuantity.isEmpty()) {
            String message = getString(R.string.required);
            etQuantify.setError(message);
            isSuccess = false;
        }
        if (strValue.isEmpty()) {
            String message = getString(R.string.required);
            etValue.setError(message);
            isSuccess = false;
        }
        if (isSuccess) {
            float value = Float.parseFloat(strValue);
            int quantify = Integer.parseInt(strQuantity);
            Product product = new Product(id, name, description, quantify, value);
            updateProductInDB(product);
            finish();
        }

    }

    private void updateProductInDB(Product product) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(product);
        realm.commitTransaction();
    }
}
