package com.example.comprasjc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.comprasjc.models.Product;

import io.realm.Realm;

public class ConfirmProductActivity extends AppCompatActivity {

    private Product product;
    private TextView tvName;
    private TextView tvDescription;
    private TextView tvQuantify;
    private TextView tvValue;

    private Button btnConfirm;
    private Button btnReject;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_product);
        realm = Realm.getDefaultInstance();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.confirmProduct);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        loadViews();

        product = (Product) getIntent().getSerializableExtra("product");
        loadProduct();
    }

    private void loadProduct() {
        tvName.setText(product.getName());
        tvDescription.setText(product.getDescription());
        tvQuantify.setText(String.valueOf(product.getQuantify()));
        tvValue.setText(String.valueOf(product.getValue()));
    }

    private void loadViews() {
        tvName = findViewById(R.id.tv_name);
        tvDescription = findViewById(R.id.tv_description);
        tvQuantify = findViewById(R.id.tv_quantify);
        tvValue = findViewById(R.id.tv_value);
        btnConfirm = findViewById(R.id.btn_confirm);
        btnReject = findViewById(R.id.btn_reject);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmProduct();
            }
        });
        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void confirmProduct() {
        Toast.makeText(this, "Producto guardado", Toast.LENGTH_SHORT).show();
        addProductInDB(product);
        Intent intent = new Intent(this, ProductActivity.class);
        startActivity(intent);
        finish();
    }

    private void addProductInDB(Product product) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(product);
        realm.commitTransaction();
    }

}
