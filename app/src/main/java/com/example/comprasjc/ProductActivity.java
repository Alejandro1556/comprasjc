package com.example.comprasjc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.comprasjc.models.Product;
import com.example.comprasjc.models.ProductAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import io.realm.Realm;

public class ProductActivity extends AppCompatActivity implements ProductAdapter.ProductAdapterListener{

    private RecyclerView recyclerView;
    private Realm realm;
    private ProductAdapter productAdapter;
    private LinearLayout llRecyclerView;
    private LinearLayout llViewNull;
    private TextView tvValueBuyTotal;

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        mAuth = FirebaseAuth.getInstance();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.startTitle);
        }
        realm = Realm.getDefaultInstance();
        llRecyclerView = findViewById(R.id.llRecyclerView);
        llViewNull = findViewById(R.id.llViewNull);
        recyclerView = findViewById(R.id.recycler_view);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToAddProduct();
            }
        });
        listSize();
        loadProducts();
    }

    private void listSize() {
        List<Product> productos = getProducts();

        if (productos.size() == 0) {
            llViewNull.setVisibility(View.VISIBLE);
            llRecyclerView.setVisibility(View.GONE);
        } else {
            llViewNull.setVisibility(View.GONE);
            llRecyclerView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getString(R.string.searchProduct));



        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String filter) {

                if (filter.isEmpty()||filter.length()==0){
                    getProducts();
                    List<Product> productsFiltered = realm.where(Product.class).findAll();

                    productAdapter.setProducts(productsFiltered);
                }else{
                    List<Product> productsFiltered = realm.where(Product.class)
                            .beginGroup()
                            .contains("name", filter)
                            .or()
                            .contains("description", filter)
                            .endGroup()
                            .findAll();

                    productAdapter.setProducts(productsFiltered);
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_logout:
                mAuth.signOut();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                Toast.makeText(ProductActivity.this, getString(R.string.logout), Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateProducts();
    }

    private void loadProducts() {
        List<Product> products = getProducts();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        productAdapter = new ProductAdapter(this, products,this);
        recyclerView.setAdapter(productAdapter);
    }

    private List<Product> getProducts() {
        tvValueBuyTotal = findViewById(R.id.tv_valueBuyTotal);
        List<Product> products = realm.where(Product.class).findAll();
        float acum = 0;
        for (int i = 0; i<products.size() ; i++) {
            Product product = products.get(i);
            if (product != null) {
                acum = acum + (product.getQuantify() * product.getValue());
            }
        }
        tvValueBuyTotal.setText(String.valueOf(acum));
        return products;
    }

    @Override
    public void deleteProduct(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(R.string.deleteProduct);
        builder.setMessage(R.string.askDeleteProduct);
        builder.setPositiveButton(R.string.afirmativeAnswer, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                realm.beginTransaction();
                Product product = realm.where(Product.class).equalTo("id", id).findFirst();
                if (product != null) {
                    product.deleteFromRealm();
                }
                realm.commitTransaction();
                Toast.makeText(ProductActivity.this, R.string.succesDelete, Toast.LENGTH_LONG).show();
                updateProducts();
                listSize();
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.negativeAnswer, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.create().show();
        updateProducts();
    }


    @Override
    public void editProduct(String id) {
        Intent intent = new Intent(this, EditProductActivity.class);
        intent.putExtra("productId", id);
        startActivity(intent);
    }

    private void updateProducts() {
        productAdapter.updateProducts(getProducts());
    }

    private void goToAddProduct(){
        Intent intent = new Intent(this, AddProductActivity.class);
        startActivity(intent);
    }

}
