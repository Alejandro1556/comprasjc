package com.example.comprasjc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    private EditText etNameUser;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etNumberCellphone;
    private Button btnRegister;

    private String nameUser;
    private String email;
    private String password;
    private String numberCellphone;

    FirebaseAuth mAuth;
    DatabaseReference usersDatabase;

    private LinearLayout llFormAdd;
    private LinearLayout llProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();
        usersDatabase = FirebaseDatabase.getInstance().getReference();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.startTitle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
            });
        }

        llFormAdd = findViewById(R.id.llFormAdd);
        llProgressBar = findViewById(R.id.llProgressBar);

        etNameUser = findViewById(R.id.et_name);
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        etNumberCellphone = findViewById(R.id.et_number_cellphone);
        btnRegister = findViewById(R.id.btn_register);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nameUser = etNameUser.getText().toString();
                email = etEmail.getText().toString();
                password = etPassword.getText().toString();
                numberCellphone = etNumberCellphone.getText().toString();

                if (!nameUser.isEmpty() && !email.isEmpty() && !password.isEmpty() && !numberCellphone.isEmpty()){

                    if (password.length() >= 6) {
                        llProgressBar.setVisibility(View.VISIBLE);
                        llFormAdd.setVisibility(View.GONE);
                        registerUser();
                    }else{
                        Toast.makeText(SignUpActivity.this, getString(R.string.propertiesPass), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(SignUpActivity.this, getString(R.string.valuesNull), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void registerUser(){
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
               if (task.isSuccessful()){
                   String id = mAuth.getCurrentUser().getUid();

                   Map<String, Object> map = new HashMap<>();
                   map.put("name", nameUser);
                   map.put("email", email);
                   map.put("password", password);
                   map.put("numberCellphone", numberCellphone);

                    usersDatabase.child("Users").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task2) {
                            if (task2.isSuccessful()){
                                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                                finish();
                                llFormAdd.setVisibility(View.VISIBLE);
                                llProgressBar.setVisibility(View.GONE);
                            }else{
                                llFormAdd.setVisibility(View.VISIBLE);
                                llProgressBar.setVisibility(View.GONE);
                                Toast.makeText(SignUpActivity.this, getString(R.string.noRegister), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
               }else{
                   llFormAdd.setVisibility(View.VISIBLE);
                   llProgressBar.setVisibility(View.GONE);
                   Toast.makeText(SignUpActivity.this, getString(R.string.noRegister), Toast.LENGTH_LONG).show();
               }
            }
        });
    }
}
