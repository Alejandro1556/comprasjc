package com.example.comprasjc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.comprasjc.models.Product;

import java.util.UUID;

public class AddProductActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etDescription;
    private EditText etQuantify;
    private EditText etValue;
    private Button btnSave;
    private Button btnReject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        loadViews();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.addProducts);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),ProductActivity.class));
                }
            });
        }
    }

    private void loadViews() {
        etName = findViewById(R.id.et_name);
        etDescription = findViewById(R.id.et_description);
        etQuantify = findViewById(R.id.et_quantify);
        etValue = findViewById(R.id.et_value);

        btnSave = findViewById(R.id.btn_confirm);
        btnReject = findViewById(R.id.btn_reject);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save(view);
            }
        });
        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void save(View view) {
        String name = etName.getText().toString().trim();
        String description = etDescription.getText().toString().trim();
        String strQuantity = etQuantify.getText().toString().trim();
        String strValue = etValue.getText().toString().trim();

        boolean isSuccess = true;

        if (name.isEmpty()) {
            String message = getString(R.string.required);
            etName.setError(message);
            isSuccess = false;
        }
        if (description.isEmpty()) {
            String message = getString(R.string.required);
            etDescription.setError(message);
            isSuccess = false;
        }
        if (strQuantity.isEmpty()) {
            String message = getString(R.string.required);
            etQuantify.setError(message);
            isSuccess = false;
        }
        if (strValue.isEmpty()) {
            String message = getString(R.string.required);
            etValue.setError(message);
            isSuccess = false;
        }
        if (isSuccess) {
            float value = Float.parseFloat(strValue);
            int quantify = Integer.parseInt(strQuantity);
            String id = UUID.randomUUID().toString();
            Product product = new Product(id, name, description, quantify, value);
            openConfirmActivity(product);
        }

    }

    private void openConfirmActivity(Product product) {
        Intent intent = new Intent(this, ConfirmProductActivity.class);
        intent.putExtra("product", product);
        startActivity(intent);    }

}
